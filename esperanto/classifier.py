base = open( "esperanto-english-orig.csv", "r" )

lines = base.readlines()

nouns = []
verbs = []
adverbs = []
adjectives = []

niceCsv = open( "esperanto-english.csv", "w" )
niceCsv.write( "esperanto,english\n" )

for line in lines:
    columns = line.split( "|" )
    columns[0] = columns[0].strip()
    columns[1] = columns[1].strip()
    
    print( columns )
    
    # Look at final letter of Esperanto word
    
    if ( columns[0][-1] == "o" ):
        nouns.append( columns )
    elif ( columns[0][-1] == "a" ):
        adjectives.append( columns )
    elif ( columns[0][-1] == "i" ):
        verbs.append( columns )
    elif ( columns[0][-1] == "e" ):
        adverbs.append( columns )
    elif ( columns[0][-2:] == "oj" ):
        nouns.append( columns )
        
    niceCsv.write( "\"" + columns[0] + "\", \"" + columns[1] + "\"\n" )
        
# Write new text file
fileNouns = open( "esperanto-english_nouns.csv", "w" )
fileNouns.write( "esperanto,english\n" )

fileVerbs = open( "esperanto-english_verbs.csv", "w" )
fileVerbs.write( "esperanto,english\n" )

fileAdjectives = open( "esperanto-english_adjectives.csv", "w" )
fileAdjectives.write( "esperanto,english\n" )

fileAdverbs = open( "esperanto-english_adverbs.csv", "w" )
fileAdverbs.write( "esperanto,english\n" )

for noun in nouns:
    fileNouns.write( "\"" + noun[0] + "\", \"" + noun[1] + "\"\n" )

for verb in verbs:
    fileVerbs.write( "\"" + verb[0] + "\", \"" + verb[1] + "\"\n" )

for adjective in adjectives:
    fileAdjectives.write( "\"" + adjective[0] + "\", \"" + adjective[1] + "\"\n" )

for adverb in adverbs:
    fileAdverbs.write( "\"" + adverb[0] + "\", \"" + adverb[1] + "\"\n" )