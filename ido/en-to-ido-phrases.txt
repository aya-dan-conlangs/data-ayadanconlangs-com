Yes.			- Yes.
No.			- No.
Thank you.		- Danko. Me dankas.
Thanks.			- Danko. Me dankas.
Please.			- Me pregas.
Not at all.		- Tute ne.
Yes, certainly.		- Yes, certe.
Certainly not.		- Certe ne.
Without a doubt.	- Sen dubo.
No doubt.		- Sen dubo.
Obviously.		- Esas evidenta.
Excuse me.		- Pardonez me.
Take care!		- Havez zorgo.
Look out!		- Zorgez!

I am human.		- Me esas homo.
I am happy.		- Me esas felica.
You are a cat.		- Vu esas kato.
You are purple.		- Vu esas purpura.
I have cheese.		- Me havas fromajo.
Do you have bread?	- Kad vu havas pano?
I want to buy a cake.	- Me volas komprar kuko.

Do you speak Esperanto?	- Kad vu parolas Esperanto?
I know Spanish.		- Me savas la Hispana.
I can't speak English.	- Me ne povas parolar la Angla.

You speak too fast.	- Vu parolas tro rapide.
Speak slowly.		- Parolez malrapide.

Do you understand?	- Kad vu komprenas?
I don't understand.	- Me ne komprenas.
Say it again.		- Re-dicez ol.
What did you say?	- Quon vu dicis?

I'm sorry.		- Me rigretas.
I am sorry.		- Me rigretas.

How are you?		- Quale vu standas?
How do you do?		- Quale vu standas?

Any news?		- Kad esas irgo nuva?
No, nothing new.	- No, nulo nuva.

Where do you live?	- Ube vu lojas?

I am busy.		- Me esas okupata.


Hello.			- Saluto.
Good bye.		- Adyo.
Goodbye.		- Adyo.
Bye.			- Adyo.
See you later.		- Til la re-vido.

Where is that?		- Ube ol esas?
Where is it?		- Ube ol esas?
Is it far from here?	- Kad ol esas for hike?
How far is it?		- Quante ol distas?

Do you know Mr. Zam?	- Kad vu konocas Sioro Zam?
