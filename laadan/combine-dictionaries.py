import json

# Read the json files

englishFile = open( "dictionary_laadan-to-english_living.json", "r" )
spanishFile = open( "dictionary_laadan-to-spanish.json", "r" )

englishData = json.loads( englishFile.read() )
spanishData = json.loads( spanishFile.read() ) 

combinedDictionary = {}

wordDuplicates = {}

for entry in englishData:
    # Make sure to allow duplicates
    laadanWord = entry["láadan"]
    
    if ( laadanWord not in wordDuplicates ):
        wordDuplicates[ laadanWord ] = 1
    else:
        wordDuplicates[ laadanWord ] += 1
        
    laadanWord = laadanWord + "-" + str( wordDuplicates[ laadanWord ] )
    
    if ( entry["láadan"] == "-thi" ):
        print( laadanWord )
    
    combinedDictionary[laadanWord] = {}
    combinedDictionary[laadanWord]["láadan"] = entry["láadan"]
    combinedDictionary[laadanWord]["english"] = {}
    combinedDictionary[laadanWord]["english"]["translation"] = entry["english"]
    combinedDictionary[laadanWord]["english"]["description"] = entry["description"]
    combinedDictionary[laadanWord]["english"]["classification"] = entry["classification"]
    combinedDictionary[laadanWord]["english"]["notes"] = entry["notes"]
    combinedDictionary[laadanWord]["english"]["breakdown"] = entry["word breakdown"]

for entry in spanishData:
    #print( "" )
    #print( entry )
    laadanWord = entry["láadan"] + "-1"
    combinedDictionary[laadanWord]["español"] = {}
    combinedDictionary[laadanWord]["español"]["translation"] = entry["español"]
    
# Write it back out to a JSON and CSV file
jsonFile = open( "multilingual_laadan_dictionary.json", "w" )
json.dump( combinedDictionary, jsonFile, indent=4 )
